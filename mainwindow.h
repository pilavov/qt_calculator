#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    double firstParametr;
    double result_number_press = 0;
    double firstParametr_press = 0;
    int lastKey_press = 0;
    bool wasEqualPressed = false;


    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *e);
private:
    Ui::MainWindow *ui;
private slots:
    void digits_numbers();
    void push_dot();
    void operations();

    void on_equals_clicked();
    void on_AC_clicked();
    void on_plusandminus_clicked();
    void on_procent_clicked();
};

#endif // MAINWINDOW_H
