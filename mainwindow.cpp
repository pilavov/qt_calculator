#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWidget>
#include <QKeyEvent>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    setMinimumSize(330,500);
    setMaximumSize(330,500);
    setWindowTitle("Calculator");

    connect(ui -> zero, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> one, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> two, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> three, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> four, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> five, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> six, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> seven, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> eight, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> nine, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui -> dot, SIGNAL(clicked()), this, SLOT(push_dot()));
    connect(ui-> plus , SIGNAL(clicked()), this, SLOT (operations()));
    connect(ui-> multiply , SIGNAL(clicked()), this, SLOT (operations()));
    connect(ui-> divide , SIGNAL(clicked()), this, SLOT (operations()));
    connect(ui-> minus , SIGNAL(clicked()), this, SLOT (operations()));
    connect(ui-> equals , SIGNAL(clicked()), this ,SLOT(on_equals_clicked()));




    ui->plus->setCheckable(true);
    ui->multiply->setCheckable(true);
    ui->divide->setCheckable(true);
    ui->minus->setCheckable(true);


}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::keyPressEvent(QKeyEvent *e) {

    double result;
    QString result_label ;
    if (e->key() > 47 && e->key() < 58){

        if(wasEqualPressed){
            ui->result_show->clear();
            wasEqualPressed = false;
        }


      result_number_press = (ui -> result_show ->text() + QString::number(e->key() - 48) ).toDouble();
      result_label = QString::number(result_number_press,'g',12);

      ui->result_show->setText(result_label);
    }

    //divide-47 mult-42 plus-43 minus-45
    else if (e->key() == 42 || e->key()==43 || e->key() == 45 || e->key() == 47 ){
        firstParametr_press = (ui -> result_show ->text()).toDouble();
        lastKey_press = e->key();
        ui->result_show->clear();
    }
    // equals = 61
    //enter = 16777220
    else if (e->key() == 61 || e->key() == 16777220) {
        wasEqualPressed = true;
        if (lastKey_press == 43){
            result = firstParametr_press + (ui -> result_show ->text()).toDouble();
            //firstParametr_press = (ui->result_show->text().toDouble());
            ui->result_show->setText(QString::number(result));

          }
        else if (lastKey_press == 45) {
            result = firstParametr_press - (ui->result_show ->text()).toDouble();
            ui->result_show->setText(QString::number(result));
        }
        else if (lastKey_press == 42) {
            result = firstParametr_press * (ui->result_show ->text()).toDouble();
            ui->result_show->setText(QString::number(result));
        }
        else if (lastKey_press == 47) {
            result = firstParametr_press / (ui->result_show ->text()).toDouble();
            ui->result_show->setText(QString::number(result));
        }
    }
    else if (e->key() == Qt::Key::Key_Escape) {
        this->close();
    }
}


void MainWindow::digits_numbers(){

    if(wasEqualPressed){
        ui->result_show->clear();
        wasEqualPressed = false;
    }

    double result_number = 0;
    QString result_label ;
    QPushButton * button = (QPushButton *) sender();

    result_number = (ui -> result_show ->text() + button->text()).toDouble();
    result_label = QString::number(result_number,'g',12);

    ui->result_show->setText(result_label);
}


void MainWindow::push_dot(){
        if(!ui -> result_show->text().contains('.')){
            ui->result_show->setText(ui->result_show->text() + ".");
        }
}


void MainWindow::operations(){
    QPushButton * button = (QPushButton *) sender();

    firstParametr = ui->result_show->text().toDouble();
    ui ->result_show->clear();
    button->setChecked(true);
}

void MainWindow::on_equals_clicked()
{

    wasEqualPressed = true;

    double secondParametr = ui->result_show->text().toDouble();
    double result;
    if (ui->plus->isChecked() || lastKey_press == 43){
        result = firstParametr + secondParametr ;
        ui->result_show->setText(QString::number(result));
        ui->plus->setChecked(false);
    }
    else if (ui ->minus ->isChecked()) {
        result = firstParametr - secondParametr ;
        ui->result_show->setText(QString::number(result));
        ui->minus->setChecked(false);
        }
    else if (ui->divide ->isChecked()) {
        if (secondParametr < 0.00001){
                ui->result_show->setText("infinity");
            }
        else {
        result = firstParametr / secondParametr ;
        ui->result_show->setText(QString::number(result));
        }

        ui->divide->setChecked(false);
    }

    else if (ui ->multiply ->isChecked()) {
        result = firstParametr * secondParametr ;
        ui->result_show->setText(QString::number(result));
        ui->multiply->setChecked(false);
        }
}

void MainWindow::on_AC_clicked()
{
    ui ->result_show->clear();
    ui ->result_show ->setText("0");
}

void MainWindow::on_plusandminus_clicked()
{
    double number = ui -> result_show ->text().toDouble();
    number *= -1;
    ui->result_show->setText(QString::number(number,'g',12));
}

void MainWindow::on_procent_clicked()
{
    double number = ui -> result_show ->text().toDouble();
    number /= 100;
    ui->result_show->setText(QString::number(number,'g',12));
}
